IDE

- !!Vscode - https://code.visualstudio.com/
- !!WebStorm - https://www.jetbrains.com/ru-ru/webstorm/
- SublimeText 

Для перегляду макетів
- Figma - https://www.figma.com/

Макети для практики - https://www.uistore.design/categories/landing-pages/page/4/

VsCode extensios: 
- auto rename tag
- auto close tag 
- auto complete tag
- auto import
- better comments 
- bracket pair colorizer 
- ccs peek
- css navigation
- html css support
- html/css/javascript snippets
- intlliSense for CSS class name
- open in browser
- path intellisenss
- prettier !!!!
- eslint !!!!
- live server


Сайти для картинок:

- https://www.pexels.com/ru-ru/
- https://unsplash.com/

Градієнти - https://webgradients.com/

FRONTEND

- HTML5 - мова розмітки
    - <name> CONTEXT </name>
    - <img/>




Можливо комусь буде цікаво😄, мій інстаграм - https://www.instagram.com/yakovenkoandy/
